# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from competeraAmazon.items import CompeteraAmazonItemTequila

class AmazontequilaSpider(CrawlSpider):
    name = 'amazonTequila'
    allowed_domains = ['amazon.co.uk']
    start_urls = ['https://www.amazon.co.uk/b/ref=s9_acss_bw_ct_370CT16_ct5_a3_w?_encoding=UTF8&node=359901031&pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=merchandised-search-4&pf_rd_r=B3H21CMJPXEFF134KAGJ&pf_rd_t=101&pf_rd_p=e8267e36-8a36-56b6-89f8-182dedf838ec&pf_rd_i=358583031']
    # rules = (
    #     Rule(LinkExtractor(), callback='parse_item', follow=True),
    # )

    def parse(self, response):
        for item in response.css('li.s-result-item'):
            url = item.css('div.a-spacing-mini  a.a-link-normal::attr("href")').extract_first()
            yield scrapy.Request(response.urljoin(url), callback=self.parse_item)

        next_page = response.css('a#pagnNextLink::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(response.urljoin(next_page), self.parse)

    def parse_item(self, response):
        #ask about formatting !!!!!
        #getting item properties
        item_name = response.css('span#productTitle::text').extract_first().replace('  ', '').replace('\n', '')
        if response.css('span#priceblock_ourprice::text').extract_first() is None:
            item_price = None
            item_avail = 'n/a'
        else:
            item_price = response.css('span#priceblock_ourprice::text').extract_first()[1:]
            item_avail = 'av'
        item_currency = response.css('span.a-color-price::text').extract_first().replace('  ', '').replace('\n', '')[0]
        item_categories = [
            category.replace('\n', '').replace(' ', '') for category
            in response.css('div#wayfinding-breadcrumbs_feature_div ul li span.a-list-item a.a-link-normal::text ').extract()]
        item_description = response.css('div#productDescription p::text').extract_first().replace('\n', '').replace('\t', '')
        item_size = None
        item_region = None
        for row in response.css('div.pdTab table tr '):
            if row.css('td.label::text').extract_first() == 'Volume':
                item_size = row.css('td.value::text').extract_first()
            elif row.css('td.label::text').extract_first() == 'Country of origin':
                item_region = row.css('td.value::text').extract_first()

        #debug
        # print 'results', item_name, item_region, item_size, item_price, item_currency, item_avail

        #saving to item
        item = CompeteraAmazonItemTequila(name=item_name,
                                   price=item_price,
                                   currency=item_currency,
                                   category=item_categories,
                                   avail=item_avail,
                                   description=item_description,
                                   size=item_size,
                                   region=item_region,
                                   )
        yield item
